# Overview

`reduxBaas` connects `redux` store to a reduxBaas data storage via an API service.
    
    

# 1. First configure your API endpoints
Go To [SimpleBaas API Site](https://simplebaas.com) and create developer account (It should take about a minute).

You will get a script tag you will need to put in the `<HEAD>` section of your `index.html`.
It should look like this:
```
 <script baas-public-api-key="<*appKey>" src="https://d1590ot8b7g2in.cloudfront.net/sdk.min.js" type="text/javascript"></script>
```
In order to save `store` data you have to be logged in

`email/password` - same as when you created an account on https://api.enkoda.pro
```
// GLOBAL
window.SIMPLE_BAAS.login(email, password).then(...) 
// with baasRedux import
login(email, password).then(...) 
```
You can find a `window.SIMPLE_BAAS.login` example in [src/components/login.js](https://gitlab.com/jarek707/redux-baas/blob/master/src/components/Login.js#L75)

<br />

# 2. In your React Project:


## 2.1 Install (copy files) from `reduxBaas` directory to your source

Copy the contents of the directory (Files: index.js, reduxBaas.js)
[src/redux-baas](https://gitlab.com/jarek707/redux-baas/blob/master/src/redux-baas)
into your `src/redux-baas` directory

(sorry, no `npm install` yet)
<br />
<br />

## 2.2 Configure `redux-baas/index.js`
In `redux-baas/index.js` define persistent data endpoints in `reduxBaasConfig` object.

Import `reducers` and, optionally, import initial store data and `middlewares`.

Then call `reduxBaas.setConfig().createStore()` with the usual params as you would with `redux` `createStore()`.

### `redux-baas/index.js` (here you setup your configuration and create a store)


```
import {createStore} from 'redux';

import {reduxBaas, sdk, setReducers} from './reduxBaas';
import reducers from '../containers/reducers.js';

// START
let reduxBaasConfig = {
    shop:   {
        populateOnLoad: true,
        actions: []
    },
    basket: {
        populateOnLoad: false,
        actions: ['ADD_TO_CART', 'REMOVE_FROM_CART']
    },
    login: {
        populateOnLoad: false,
        actions: ['LOGGED_IN_SUCCESS', 'LOGGED_OUT_SUCCESS']
    }
}

//
// Option 1 - NOTE: reduxBaas.connect() returns redux store - its argument
//
let store = reduxBaas.connect(
    createStore(
        setReducers(reducers, reduxBaasConfig),
        {}
        //,applyMiddleware(thunk)
    )
);

/*
//
// Option 2 - one liner - createStore (below) uses standard redux createStore, you can also pass
//                        additional initialState and middlewares arguments
//
let store = reduxBaas.setConfig(reduxBaasConfig).createStore(reducers);
*/
// END

let login           = sdk.login;
let logout          = sdk.logout;
let heartbeat       = sdk.heartbeat;
let refreshFromBaas = reduxBaas.refresh;

export {store, sdk, login, logout, heartbeat, refreshFromBaas};
```

<br />

## Config (`reduxBaasConfig` object)
An object with keys corresponding to a subset of your reducer keys. 
- `populateOnLoad: true/false` - will populate redux store from API when the App starts up.
- `actions: []` - an array of standard reducer action keys for which the data should be persisted to API storage.

<br />

## Using baas SDK API (optional)
`reduxBaas` additionally exports an `sdk` object. Using this object you can access full range of features available from `baasSimple` API service such as

- Creating additional user accounts
- Working with additional data endpoints
- ... more [See simpleBaas documentation](https://gitlab.com/jarek707/key-api-sample/blob/master/DOCUMENTATION.md)


### Refreshing (reloading from baas)

Thus far you've been shown how to configure `reduxBaasConfig` object, create a store, initialize redux store with baas data and persist redux store data to the baas service when the App is loading.
Some of the data may not be accessible at the time of page load (private keys) unless you are logged in.  

You can update redux store from baas API at any time.
```
import {refreshFromBaas} from '../redux-baas';

refreshFromBaas('basket');
```
Please see an example at [src/components/login.js](https://gitlab.com/jarek707/redux-baas/blob/master/src/components/Login.js#L75)
