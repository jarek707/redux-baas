const emptyInitState = [];

const inventoryReducer = (state = emptyInitState, action) => {
    let retState = [...state];

    if (state && action.payload) {
        let id = action.payload.id;

        if (action.type === 'ADD_TO_CART')
            retState.forEach(s => s.id === id && s.qty > 0 && s.qty--);

        if (action.type === 'REMOVE_FROM_CART')
            retState.forEach( s => {
                s.id === id && s.qty++ 
            });
    }

    return retState;
}

const basketReducer = (state=emptyInitState, action) => {

    let retState = [...state];

    if (state === undefined) {
        return state;
    }

    if (action.type === 'ADD_TO_CART') {
        const data  = action.payload;
        const found = state.filter(book => data.id === book.id )

        if (found.length === 0) {
            retState = [ ...state, data ];
        } else {
            const newState = [...state].map(book =>{
                if (book.id === data.id) {
                    book.qty = Number(book.qty) + 1;
                }
                return book
            })

            retState = newState
        }
    }

    if (action.type === "REMOVE_FROM_CART") {
        retState = [...state].filter(book => {
            if (action.payload.id === book.id && book.qty > 1) {
                book.qty--;
                return book;
            } else {
                return book.id !== action.payload.id;
            }
        })
    }

    if (action.type === 'LOGGED_OUT_SUCCESS') {
        while (retState.length) retState.pop();
    }

    retState = retState.sort((a, b) => a.id > b.id ? 1 : -1);

    return retState;
}

const loginReducer = (state={}, action) => {
    action.payload || (action.payload = {});

    let newState = Object.assign({} , state);

    if (action.type === 'LOGGED_IN_SUCCESS') {
        Object.assign(newState, action.payload, {
            loggedIn:     true,
            loginAttempt: false
        });
    } else if (action.type === 'LOGIN_ATTEMPT') {
        newState.loginAttempt = true;
    } else if (action.type === 'LOGIN_ATTEMPT_FAILED') {
        Object.assign( newState, {
            loginAttempt: false,
            loginFailed : true
        });
    } else if (action.type === 'LOGGED_OUT_SUCCESS') {
        Object.assign( newState, {
            dataLoaded : false,
            email      : '',
            loggedIn   : false,
            loginAttempt : false
        });
    } else if (action.type === 'INITIAL_BAAS_LOAD') {
        newState.dataLoaded = true;
    }

    return newState;
}

const routeReducer = (state={}, action) => {
    let newState = Object.assign({} , state);
    
    if (action.type === 'ROUTE_TO') {
        Object.assign( newState, {
            route: action.payload
        });
    }
    return newState;
}

const initReducer = (state={}, action) => {
    let newState = {...state}
    
    if (action.type === 'CONFIG_DATA') {
        Object.assign( newState,action.payload);
    }
    return newState;
}

export default {
    init:   initReducer,
    shop:   inventoryReducer,
    basket: basketReducer,
    login:  loginReducer,
    route:  routeReducer
}
