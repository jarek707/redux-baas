import { connect } from 'react-redux'
import Go from '../components/Go.js';

const mapStateToProps = state => {
    return {
        login: state.login
    }
}

export default connect(
    mapStateToProps
)(Go)
