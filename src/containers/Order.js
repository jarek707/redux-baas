import { connect } from 'react-redux'
import Order from '../components/Order.js';

const mapStateToProps = state => {
    return {
        basket: state.basket,
        shop:   state.shop
    }
}

export default connect(
    mapStateToProps
)(Order)
