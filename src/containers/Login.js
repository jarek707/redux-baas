import {connect} from 'react-redux'
import Login from '../components/Login.js';

const mapStateToProps = state => {
    return {
        route: state.route,
        login: state.login
    }
}

const mapDispatchToProps = dispatch => {
    return {
        attempt:      () => dispatch({type: 'LOGIN_ATTEMPT'}),
        failed:       () => dispatch({type: 'LOGIN_ATTEMPT_FAILED'}),
        loggedOut: state => dispatch({type: 'LOGGED_OUT_SUCCESS', payload: state}),
        loggedIn:  state => dispatch({type: 'LOGGED_IN_SUCCESS',  payload: state})
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login)
