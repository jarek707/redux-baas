import {createStore, applyMiddleware} from 'redux'; 

import {reduxBaas, sdk, setReducers} from './reduxBaas';
import reducers from '../containers/reducers.js';
import thunk from 'redux-thunk';

// START
let reduxBaasConfig = {
    shop:   { 
        populateOnLoad: true,
        actions: []
    },
    basket: { 
        populateOnLoad: true,
        actions: ['ADD_TO_CART', 'REMOVE_FROM_CART']
    },
    login: {
        populateOnLoad: false,
        actions: ['LOGGED_IN_SUCCESS', 'LOGGED_OUT_SUCCESS']
    }
}

//
// Option 1 - NOTE: reduxBaas.connect() returns redux store - its argument
//
let store = reduxBaas.connect( 
    createStore( 
        setReducers(reducers, reduxBaasConfig),
        {}
        ,applyMiddleware(thunk)
    )
);

/*
//
// Option 2 - one liner - createStore (below) uses standard redux createStore, you can also pass 
//                        additional initialState and middlewares arguments
//
let store = reduxBaas.setConfig(reduxBaasConfig).createStore(reducers);
*/
// END

let login           = sdk.login;
let logout          = sdk.logout;
let heartbeat       = sdk.heartbeat;
let refreshFromBaas = reduxBaas.refresh;

export {store, sdk, login, logout, heartbeat, refreshFromBaas};
