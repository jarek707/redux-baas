if (typeof window.SIMPLE_BAAS == 'undefined') {
    (function(window) {

        let frameSrc  = 'https://simplebaas.com/sdk.html';
        let frame     = null;
        let clientUid = '';
        let loaded    = false;
        let thenCbs   = {};
        let deferred  = [];

        window.receiveInParent = evt => {
            let params = null;
            try       { 
                if (Object.keys(evt.data).indexOf('reactBuildType') > -1 ) {
                    console.log( 'BAAS detect: React...');
                    params = null;
                } else if (evt.data.type && evt.data.type.substring(0,7) === 'webpack') {
                    console.log( 'BAAS detect: Probably Angular...',  evt.data.data ? evt.data.data : '');
                } else {
                    params = JSON.parse(evt.data) 
                }
            }
            catch (e) { console.warn('No valid evt.data...ignoring\n', e, evt) }

            if (params) {
                let {ts, data} = params;
                let doResolve  = data.success || typeof data.success == 'undefined';
                let content    = data.data ? data.data : data;

                thenCbs[ts] && thenCbs[ts][doResolve ? 0 : 1](content);
                handleDeferred();
                delete thenCbs[ts];
            }
        }

        let handleDeferred = () => {
            while (deferred.length)
                frame.contentWindow.postMessage(deferred.shift(), '*')
        }

        let makeIFrame = () => {
            frame     = document.createElement('iframe');
            frame.src = frameSrc;

            frame.style.position = 'fixed';
            frame.style.top      = '-1000px';
            frame.style.left     = '-1000px';
            frame.style.height   = '0px';
            frame.style.width    = '0px';

            document.body.appendChild(frame);
            frame.addEventListener('load', () => handleDeferred( loaded = true ) );
        }

        window.addEventListener('message', window.receiveInParent, false);
        window.addEventListener('load',    makeIFrame);

        let call = (url, method, data = null) => new Promise ( (y, n) => {
            let now = new Date().getTime() + '-' +Math.round(Math.random()*1000);

            thenCbs[now] = [y, n];

            let load = JSON.stringify({
                "ts":       now,
                "url":      url,
                "method":   method,
                "origin":   document.location.origin,
                "data":     data
            });

            if (loaded)
                frame.contentWindow.postMessage(load, '*');
            else {
                deferred.push(load);
            }
        });

        let setPostData = (key, content, meta) => {
            let data = {
                client_uid: clientUid,
                content:    content,
                key:        key
            }
            return Object.assign(data, meta);
        }

        let _self = window.SIMPLE_BAAS = {
            get:     url        => call(url, 'GET'),
            put:    (url, data) => call(url, 'PUT',  data),
            post:   (url, data) => call(url, 'POST', data),
            delete:  url        => call(url, 'DELETE'),
            debug:  debug       => call('debug', debug),

            // Clients START
            getClients: (userUid = '') => {
                userUid = userUid ? `/${userUid}` : '';
                return _self.get(`clients${userUid}`);
            },
            deleteClient : inClientUid => _self.delete(`clients/${inClientUid}`),
            updateClient : (uid, name, domain, cms) =>
                _self.put( 'clients', {"uid": uid, "name": name, "domain": domain, "cms": cms}),
            addClient    : (name, domain, cms) =>
                _self.post('clients', {"name": name, "domain": domain, "cms": cms}),
            // Clients END

            // Data START
            getAll:      ()                     => _self.get(`data/${clientUid}`),
            getKeys:     ()                     => _self.get(`data/keys/${clientUid}`),
            getItem:     key                    => new Promise( (y, n) => {
                _self.get(`data/${clientUid}/${key}`)
                .then( data => {
                    y(data);
                }).catch( e => n(e) );
            }),
            getCdnItem: key => new Promise( (y, n) => {
                let cdnUrl = 'https://d1590ot8b7g2in.cloudfront.net/';
                //let url    = cdnUrl + clientUid + '/' + key + '.json';
                let url    = cdnUrl + clientUid + '/' + key + '.json';

                fetch(url,  {   "mode": "cors",
                                "headers" : {
                                //"Content-Type": "application/json",
                                "Content-Type": "text/javascript",
                                    "Access-Control-Allow-Origin": "*"
                                }
                            }
                ).then(fetchItem => {
                    /*
                    fetchItem.text()
                    .then( text => console.log('this is text', text) )
                    .catch( e => console.log( 'text error', e) );
                    */

                    if (fetchItem.status === 403) {
                        n({data: 'Item not found', success: false, url: url});
                    } else {
                        return fetchItem.json()
                    }
                }
                ).then(json => y(json))
                 .catch(e => {
                    console.log(  'error on miss' , e );
                    n(e)
                 });
            }),
            deleteItem:  key                    => _self.delete(`data/${clientUid}/${key}`),
            setItem : (key, content, meta = {on_cdn: false}) => {
                return _self.post('data', setPostData(key, content, meta));
            },
            setCdnItem : (key, content, meta = {}) => new Promise( (y, n) => {
                meta.on_cdn = window.SIMPLE_BAAS.fromCdn;
                _self.post('data', setPostData(key, content, meta)).then(y).catch(n);
            }),
            isCdn: key => {
                return _self.get(`data/is-cdn/${clientUid}/${key}`);
            },
            // Data END

            // Login START
            logout   : ()                => _self.get('auth/logout'),
            heartbeat: ()                => _self.get('heartbeat'),
            login    : (email, password) => _self.get(`auth/login/${email}/${password}`),
            // Login END

            // USER - START
            createAdminUser: (email, password)=> {
                let userData = {email: email, password: password, perms: 'ADM'};
                return _self.post('users', userData);
            },
            createUser: (email, password, perms = 'REG', clientUid = '') => {
                let userData = {email: email, password: password, perms: perms, client_uid: clientUid};
                return _self.post('users', userData);
            },
            createAnyUser: (email, password, perms) => {
                let userData = {"email":email, "password": password, "perms": perms};
                return _self.post('users', userData);
            },
            updateUser: (uid, email, password, perms='ADM', clientUid = '') => {
                let userData = {
                    "uid":          uid,
                    "email":        email,
                    "password":     password,
                    "perms":        perms,
                    "client_uid":   clientUid
                };
                return _self.put('users',  userData);
            },
            deleteUser:     userUid  => _self.delete(`users/${userUid}`),
            getClientUsers: userUid  => _self.get(`users/clients/${userUid}`),
            // USER - END
            register: appUid => {
                if (appUid)
                    clientUid = appUid;
                return window.SIMPLE_BAAS;
            },
            appUid: () => clientUid
        }

        let scripts = document.getElementsByTagName('head')[0].getElementsByTagName('script');

        window.SIMPLE_BAAS.scriptTagConfig = {};
        for (let i in scripts) {
            if (scripts[i] && scripts[i].nodeName && scripts[i].nodeName.toUpperCase() === 'SCRIPT') {
                let scriptEl = scripts[i];
                let appUid   = scriptEl.getAttribute('baas-public-api-key');

                if (appUid) {
                    window.SIMPLE_BAAS.register( appUid );
                    let attributeNames = scriptEl.getAttributeNames ? scriptEl.getAttributeNames() : [];

                    attributeNames.filter( n => n !== 'type' && n !== 'src').forEach( a => {
                        let attrValue =  scriptEl.getAttribute(a); 
                        if (attrValue === 'true' || attrValue === '') attrValue = true;
                        window.SIMPLE_BAAS.scriptTagConfig[a] = attrValue === 'false' ? false : attrValue;
                    });
                    break;
                }
            }
        }
    })(window);
}

