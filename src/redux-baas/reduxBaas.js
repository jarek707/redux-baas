import {createStore, combineReducers} from 'redux'; 
import {zip} from 'rxjs';

import './sdk-mock.js';

class ReduxBaas {
    constructor() {
        this.sdk      = window.SIMPLE_BAAS;
        this.store    = null;
        this.config   = null;
        this.baasData = {};
    }

    // 
    //  setConfig - initializes this.config object from its argument
    //              and loads initial data form baas service 
    //
    //    Params:
    //      config - reduxBaas config {key: { populateOnLoad: bool, actions: []}, ... }
    //          - populateOnLoad - flag indicating if data for this key should be loaded when the page loads
    //          - actions - a list of reducer actions for which the data should be updated to baas storage
    //
    setConfig(config) { 
        this.config = config; 
        this.loadInitialBaasData();
    }

    //
    //  loadInitialBaasData - . load all data endpoints for which config has populateOnLoad === true
    //                        . this.baasData is populated with the current state on the BAAS storage
    //                        . if redux store has been initialized a dispatch is called to populate it
    //
    loadInitialBaasData() {
        let loadable = Object.keys(this.config)
            .filter(key => this.config[key].populateOnLoad)
            .map(   key => this.sdk.getItem(key));

        zip(...loadable)
        .subscribe( 
            resp => resp.map( data => this.baasData[data.key] = data.content)
        )
    }

    //
    // Refresh redux store with the most current data for a 'key'
    //
    refresh = async key => {
        this.baasData[key] = (await this.sdk.getItem( key )).content;
        this.store.dispatch({type: 'BAAS_LOAD', payload: {}})
    }

    //
    // populateLocalStore - a helper function called by reducerFn,
    //                      it populates redux store on initial load
    //
    populateLocalStore(key, state, data) {
        if (data) { // initialize state only if there was data from the baas storage
            if (data.isArray) {
                while (state.length) state.pop(); // clear the state before populating from API

                data.forEach( i => { state.push(i) })
            } else {
                state = data || {};
            }
        }
        return state;
    }

    //
    // reducerFn - It does two things:
    //
    //  1. Populates store for a given key at page load
    //  2. It excutes baas update (this.sdk.setItem) when key store has changed,
    //     if action was listed in the config
    //
    //    Params:
    //          key - reducer object key
    //
    reducerFn = key => {
        return (state, action) => {
            if (action.type === 'BAAS_LOAD') {
                state = this.populateLocalStore(key, state, this.baasData[key]);
            }

            //
            // Call baas storage with data changes for all actions types listed in reduxBaasConfig (this.config)
            //
            for (let k in this.config) {
                this.config[k].actions.forEach( a => {
                    if (a === action.type && k === key) {
                        this.sdk.setItem(key, state);
                    }
                });
            }
            return state;
        }
    }

    // 
    // Rebuild user defined reducer function:
    //    We call the user defined reducer functions and pass its returned value (store)
    //    to baas reducer function (reducerFn).
    //
    //    Params:
    //          clientFn - client reducer for key
    //
    extendReducer = (clientFn, key) => (state, action) =>
        this.reducerFn(key)(clientFn(state, action), action);
   
    //
    //  setReducers - iterates through user defined reducers and extends them with reduxBaas functionality
    //
    setReducers = (reducers, config) => {
        if (typeof config == 'object') {
            this.config = config;
        }

        for (let key in this.config) {
            reducers[key] = this.extendReducer(reducers[key], key);
        }

        return combineReducers(reducers);
    }

    //
    // connect - used when the user creates store outside of this class
    //      Params:
    //          store - user created store with redux.createStore
    //      Returns
    //          store - its argument
    //
    connect(store) {
        this.store = store;
        this.loadInitialBaasData();
        return store;
    }

    createStore(reducers, storeData = {}, middlewares = null ) {
        this.store = middlewares
            ? createStore( this.setReducers(reducers), storeData, middlewares)
            : createStore( this.setReducers(reducers), storeData);

        return this.store;
    }
}

let reduxBaas   = new ReduxBaas();
let sdk         = reduxBaas.sdk;
let setReducers = reduxBaas.setReducers;

export {reduxBaas, sdk, setReducers};
