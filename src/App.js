import React from 'react';
import './styles/App.less';
import Login from './containers/Login';
import {store} from './redux-baas';
import {Provider} from 'react-redux';
import Routes from './menuRouter.js';
import {BrowserRouter as Router} from "react-router-dom";

function App() {
    return <Provider store={store}>
            <div className="App">
                <Router>
                    <Login />
                    <Routes />
                </Router>
            </div>
        </Provider>;
}

export default App;
