class API {
    constructor() {
        this.sdk = window.DAAS_SDK_API;
    }

    get(key) {
       return new Promise( (resolve, reject) => {
            this.sdk.getItem(key)
            .then( inventory => {
                resolve(inventory);
            })
            .catch( err => console.log( err ) );
       });
    }
}

/*
sdk.login('redux', 'redux').then( logResp => {
//sdk.logout().then( logResp => {
    console.log( 'LOG RESP', logResp );

    sdk.getKeys().then( keys => {
        console.log( keys , 'keys ');
        console.log( 'GET KEYS cookie\n', document.cookie );
    })
    .catch( err => console.log( err ) );

    sdk.getItem('inventory')
    .then( inventory => {
        reduxWrap.init( null, {shop: inventory.content}, null);
        console.log( 'INVENT cookie\n', document.cookie );
    })
    .catch( err => console.log( err ) );

});

//setInterval( () => sdk.heartbeat().then( h => console.log(h) ), 10000 );
*/

let Api = new API();
export {Api};
