import React, { Component, createRef } from "react";

import {sdk}       from '../redux-baas/reduxBaas';
import {timer, from}      from 'rxjs';
import {take, filter, map, concatMap} from 'rxjs/operators';
//import {debounceTime} from 'rxjs/operators';

class LiveGame extends Component {
    constructor(props) {
        super(props);
        this.props = props;
        this.mailToRef = createRef();
        this.liveGames = [];
        this.waitHandle = null;
        this.turns = [];
        this.myColor = 'black';

        let [mine, gameHandle, live] = (localStorage.getItem('currentLiveGame') || '::').split(':');
        if (live) {
            this.state = { gameHandle: gameHandle, mine: mine, live: live }
        } else {
            this.state = { live: ''}
        }
    }

    setLiveState(gameHandle = '') {
        this.setState({gameHandle: gameHandle});

        if (gameHandle) {
            window.history.replaceState('', "Live", `go?liveGame=${gameHandle}`);
            this.props.ux().startLive();
        } else {
            window.history.replaceState('', "Not Live", `go`);
            this.props.ux().clearGame();
        }
    }

    componentDidMount() {
        this.getLiveGames().catch( e => sdk.setItem('liveGames', []) );
    }

    getLiveGames = async () => {
        let loadedGames = await sdk.getItem('liveGames'); 
        loadedGames.content && (this.liveGames = loadedGames.content);

        let slug = window.location.search.substring(1).split('=')[1];
        let idx = this.liveGames.findIndex( g => g.includes( slug ) );

        (this.state.live && idx > -1) && this.playLive(this.liveGames[idx]);
        this.forceUpdate();
    }

    mailGameHandle = () => {
        let email = this.mailToRef.current.value;
        let body  = `https://jaroslawkulikowski.com/go?liveGame=${this.state.gameHandle}`;

        if (email) {
            window.location.href = `mailto:${email}?subject=Hey,%20wanna%20play&body=${body}`;
        }
    }

    async playLiveCall(gameHandle) {
        let gameState = await sdk.getItem(gameHandle);

        let [mine]= (localStorage.getItem('currentLiveGame') || '::').split(':');

        let ownerColor    = gameState.content.meta.ownerIs;
        let opponentColor = ownerColor === 'black' ? 'white' : 'black';
        this.myColor      = mine ? ownerColor : opponentColor;

        localStorage.setItem('currentLiveGame', mine + ':' + gameHandle + ':live');
        this.setState({ gameHandle: gameHandle, mine: mine, live: 'live', ownerIs: ownerColor});

        this.setLiveState( gameHandle );
        this.replayBoard( gameState.content.game );

        this.waitForMove();
    }

    playLive(gameHandle) {
        this.playLiveCall(gameHandle)
        .catch( e => {
            let idx = this.liveGames.findIndex(s => s === gameHandle);
            if (idx > -1) {
                console.log( e, 'ERRor starting game');
                this.liveGames.splice(idx, 1);
                sdk.setItem('liveGames', this.liveGames)
                this.getLiveGames();
                this.forceUpdate();
            }
        });
    }

    startLiveCall = async () => {
        let gameHandle ='LIVE_' + new Date().toISOString().substring(5, 22).replace(/:/g,'_');  
        this.liveGames.push( gameHandle );

        localStorage.setItem('currentLiveGame', 'mine:' + gameHandle + ':live');

        let newGame = { meta: {ownerIs: Math.random() > 0.5 ? 'black' : 'white'}, game: [] };
        this.setState({ mine:       'mine',
                        gameHandle: gameHandle,
                        live:       'live',
                        ownerIs:    newGame.meta.ownerIs });

        await sdk.setItem(gameHandle, newGame);
        await sdk.setItem('liveGames', this.liveGames)

        this.setLiveState(gameHandle);
        this.playLive( gameHandle );
    }

    startLive = async () => {
        let gameState = this.startLiveCall()
        .catch( e => console.warn('ERROR Fetching new game') );

        console.log( 'STARTED GAME ', gameState );
    }

    replayBoard(stones) {
        this.props.ux().clearGame();
        stones.forEach( s => this.props.ux().addLiveStone( s ));
        this.props.ux().replay();
    }

    getTurnColor(turns) { return turns.length%2 === 0 ? 'black': 'white' };
     
    waitForMove() {
        this.props.ux().myMove( false );

        timer(1000, 1000)
        .pipe(
            concatMap( () => from(sdk.getItem(this.state.gameHandle)) ),
            map( s => s.content && s.content.game ? s.content.game : []),
            // Terminate on my turn or game exit (no game handle)
            filter(g => this.getTurnColor( g ) !== this.myColor || !this.state.gameHandle),
            take(1)
        ).subscribe( g => {
            this.props.ux().myMove( true );
            this.replayBoard( g );
        });
    }

    async makeMove(turns) {
        if (this.state.gameHandle) {
            this.props.ux().myMove( false );
            this.waitForMove();
            await sdk.setItem(this.state.gameHandle, {meta: {ownerIs: this.state.ownerIs}, game: turns});
        }
    }

    deleteLiveGame = async () => {
        sdk.deleteItem(this.state.gameHandle);

        let idx = this.liveGames.findIndex( s => s === this.state.gameHandle );
        if (idx > -1) {
            this.liveGames.splice(idx, 1);
            await sdk.setItem('liveGames', this.liveGames)
        }
        this.exitLiveGame();

        this.setLiveState();
    }

    exitLiveGame = ()  => {
        localStorage.removeItem('currentLiveGame');
        this.setState({gameHandle: '', live: ''});
        this.props.ux().exitLive();
        this.getLiveGames().catch( e => null );
    }

    clearEmail = () => {
        this.mailToRef.current.value = '';
    }

    render() {
        return <div className={`liveGame ${this.liveGames.length  > 0 ? '' : 'empty'}`}>
            {this.state.live === '' &&
                <button onClick={this.startLive}>Start Live Game</button>
            }
            {this.state.live !== '' &&
                <span>
                    <span className="flex2">
                        {this.state.mine &&
                            <button onClick={this.deleteLiveGame}>Delete</button>
                        }
                        <button onClick={this.exitLiveGame}>Exit</button>
                    </span>

                    {this.state.mine &&
                        <span>
                            <input type="text" ref={this.mailToRef} placeholder="mail to:" 
                                onFocus={this.clearEmail}
                            ></input>
                            <button onClick={this.mailGameHandle}>Email Handle</button>
                        </span>
                    }
                </span>
            }
            <hr />
            <label className="bigger">
                Live Games
                <i className="fa fa-refresh" onClick={this.getLiveGames}></i>
                <hr />
            </label>
            {this.liveGames.length === 0 &&
                <span>
                    <label className="bigger">No Live Games</label>
                </span>
            }
            <ul className="liveGameList">
                {this.liveGames.reverse() && this.liveGames.map( g =>
                    <li key={g} onClick={() => this.playLive(g)}
                        className={g === this.state.gameHandle ? 'active' : ''}
                    >
                        {g.substring(8, 22)}
                    </li>)
                }
            </ul>
        </div>
    }
}

export {LiveGame};
