import React from "react";
import {NavLink} from "react-router-dom";
import {store} from '../../redux-baas';

//
// ReduxNavLink - wrapper for react-router-dom NavLink with a dispatch 
//                to update the route in redux store
//
const ReduxNavLink = ({to, children}) =>
    <NavLink 
        to={to} 
        onClick={ () => store.dispatch({type: 'ROUTE_TO', payload: to}) }
    >
        {children}
    </NavLink>

//
// ExternaLink - wraps an <a> element with target _blank and secure rel
//
const ExternalLink = ({href, children}) =>
    <a 
        rel="noopener noreferrer"
        target="_blank" 
        href={href}
    >
        {children}
    </a>

export {ReduxNavLink, ExternalLink};

