import "./style/Go.less";
import React, { Component, createRef } from "react";
import {fromEvent}    from 'rxjs';

import {NavButtons}  from './NavButtons.js';
import {QuickMenu}   from './QuickMenu.js';
import {BaasList}    from './BaasList.js';
import {CursorIcon}  from './CursorIcon.js';
import {ContextMenu} from './ContextMenu.js';

export default class Go extends Component {
    constructor() {
        super();

        this.canvasRef      = createRef();
        this.cursorIconRef  = createRef();
        this.contextMenuRef = createRef();
    }

    componentDidMount() {
        fromEvent(this.canvasRef.current, 'mousemove')
        .subscribe( evt => {
            this.contextMenuRef.current.hide();
            this.cursorIconRef.current.setPos(evt.clientX, evt.clientY);
        });

        fromEvent(this.canvasRef.current, 'mouseout')
        .subscribe(this.cursorIconRef.current.hide);

        fromEvent(this.canvasRef.current, 'contextmenu')
        .subscribe(this.contextMenuRef.current.show);
    }

    setUx = ux => this.ux = ux;
    getUx = () => this.ux;

    changeStone = meta => this.cursorIconRef.current.setClass(meta.action, meta.nextTurn);

    render() {
        return (
                <div>
                    <QuickMenu  ux={this.getUx} />
                    <NavButtons ux={this.getUx} />

                    <div>
                        <textarea className="dropArea" id="dropArea"
                            placeholder="paste game or drop .sgf file here"
                        ></textarea>

                        <BaasList 
                            login={this.props.login}
                            changeStone={this.changeStone}
                            setUx={this.setUx}
                        />
                    </div>

                    <div id="canvases" ref={this.canvasRef}>
                        <canvas id="gridCanvas"></canvas>
                        <canvas id="mainCanvas"></canvas>
                    </div>

                    <ContextMenu ux={this.getUx} ref={this.contextMenuRef} />
                    <CursorIcon ref={this.cursorIconRef} />
                </div>
        )
    }
}
