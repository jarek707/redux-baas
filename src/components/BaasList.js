import React, { Component, createRef } from "react";

import {UX}           from './go-src';
import {sdk}          from '../redux-baas/reduxBaas';
import {fromEvent}    from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import {LiveGame} from './LiveGame.js';


class BaasList extends Component {
    constructor(props) {
        super(props);
        this.props = props;
        this.state = {"games": [], "searchedGames": []};
        this.listRef =  createRef();
        this.liveRef =  createRef();
        this.cache = {};

        document.addEventListener('keydown', evt => {
            [37, 38, 39, 40].indexOf(evt.keyCode) !== -1 && evt.preventDefault();

            switch (evt.keyCode) {
                case 37: this.ux.undoLast(evt.shiftKey ? 10:  1); break;
                case 38: this.ux.redoLast(evt.shiftKey ? 20 : 5); break;
                case 39: this.ux.redoLast(evt.shiftKey ? 10 : 1); break;
                case 40: this.ux.undoLast(evt.shiftKey ? 20 : 5); break;
                default:                                          break;
            }
        });
    }

    liveMove(turns) {
        let stones = turns.toJS().map( t => t.hash );
        this.liveRef.current.makeMove( stones );
    }

    componentDidMount() {
        this.ux = new UX();
        this.props.setUx( this.ux ); // make UX class instance available to the parent

        this.ux.registerBaas( state => {
            //console.warn( state,'state in baas list mount' );

            if (['Invalid', 'IllegalKO', 'WaitTurn'].includes(state.action)) {
                this.props.changeStone( state );
            } else if (state.action==='LiveMove') {
                this.liveMove( state.turns );
            } else if (state.action==='ButtonChanged') {
                this.props.changeStone( state );
            } else {
                let gameIndex =  this.findGameIdx(this.stateToName( state ));

                this.setState({ activeIdx: gameIndex });
                this.scrollToActiveGame();
            }
        });
        this.loadBaasListIfEmpty();

        // Allign UL list so that it aligns with LI elements in quanta
        fromEvent(this.listRef.current, 'scroll')
        .pipe(
            debounceTime(80)
        )
        .subscribe( arg => {
            arg.target.scrollTop -= arg.target.scrollTop%34;
        });
    }

    componentDidUpdate() {
        this.loadBaasListIfEmpty();
    }

    loadBaasListIfEmpty() {
        if (this.props.login.loggedIn) {
            if (this.state.games.length === 0) {
                this.loadBaasGameList();
            }
        } else {
            if (this.state.games.length !== 0) {
                this.setState({games: []});
            }
        }
    }

    async loadBaasGameList() {
        let keys = await sdk.getKeys()

        let games = keys.filter( key => key.key.substring(0,8) === 'GO_GAME_')
                    .map( k => {k.key = k.key.substring(8); return k} );

        if (games.length === 0) {
            this.setState( {hasGames: false} );
        } else {
            this.setState( {games: games.sort( (a,b) => a.key > b.key ? 1 : -1), hasGames: true} );
        }
    }

    findGameIdx(keyName) {
        return this.state.games.findIndex( g => g.key === keyName );
    }

    scrollToActiveGame() {
        setTimeout( () => {
            if (this.state.activeIdx > -1) {
                let ulEl       = this.listRef.current;
                let activeLiEl = ulEl.childNodes[this.state.activeIdx];
                let ulScroll = ulEl.scrollTop;
                let liOffset = activeLiEl.offsetTop

                if (activeLiEl && (liOffset - ulScroll >= 170 || liOffset < ulScroll)) {
                    ulEl.scrollTop = Math.max(0, liOffset - 68);
                }
            }
        }, 10);
    }

    baasName(name) {
        return name.substring(0,8) === 'GO_GAME_' ? name.substring(8)
                                                  : 'GO_GAME_' + name;
    }
    
    stateToName(meta) {
        let {EV, PB, PW, DT} = meta;
        return `${EV}-${PB}-${PW}-${DT}`.replace(/\s/g, '_');
    }

    loadBaasGame = async idx => {
        let keyName    = this.state.games[idx].key;
        let loadedGame = this.cache[keyName];

        if (!loadedGame) {
            loadedGame = (await sdk.getItem(this.baasName(keyName))).content;
            this.cache[keyName]= loadedGame;
        }

        this.ux.loadPastedGame(loadedGame);
        this.setState({activeIdx: idx});
    }

    deleteGame = async idx  => {
        if (this.state.inDelete) {
            if (this.state.activeIdx === idx)
                await sdk.deleteItem(this.baasName(this.state.games[idx].key));

            this.loadBaasGameList();
            this.setState({inDelete: false});
        } else {
            this.setState({inDelete: true, activeIdx: idx});    
            setTimeout(() => this.setState({inDelete: false}) , 2000);
        }
    }

    saveBaasGame = async () => {
        console.warn( this.ux.baasGame, ' baas game' );
        let {meta, raw} = this.ux.baasGame;
        let {EV, PB, PW, DT} = meta;

        let keyName = `${EV}_${PB}_${PW}-${DT}`;

        await sdk.setItem(this.baasName(keyName), raw);
        await this.loadBaasGameList();

        this.setState({activeIdx: this.findGameIdx(keyName)});

        this.scrollToActiveGame();
    }

    searchGame = (evt) => {
        let [first, second, third] = evt.target.value.split(' ');
        let orCondition  = second && (second.toUpperCase() ===  'OR' || second === '|' || second === '||');
        // AND condition is default, if missing and the second argument is provided then AND is used automatically
        let andCondition = second && (second.toUpperCase() === 'AND' || second === '&' || second === '&&');

        (orCondition || andCondition) && (second = third);
        
        let found = [];
        if (first.length>1) {
            found = this.state.games.filter( g => {
                let matchFirst = g.key.toUpperCase().indexOf(first.toUpperCase()) > -1; 
                if (second) {
                    let matchSecond =  g.key.toUpperCase().indexOf(second.toUpperCase()) > -1; 
                    return orCondition ? matchFirst || matchSecond : matchFirst && matchSecond;
                } else {
                    return matchFirst;
                }
            })
        }

        this.setState({searchedGames: found});
    }

    loadSearchBaasGame = async searchIdx => {
        let keyName = this.state.searchedGames[searchIdx].key;
        this.loadBaasGame(this.findGameIdx( keyName ));
    }

    resetSearch = evt => {
        this.setState({"searchedGames" : []});
        evt.target.value = '';
    }

    render() {
        let {hasGames, inDelete, activeIdx, games, searchedGames} = this.state;
        let {deleteGame, loadBaasGame, saveBaasGame} = this;

        return <div className="outterWrap plain baasList">
            <LiveGame ux={() => this.ux} ref={this.liveRef}/>

            {activeIdx === -1 && <button className="active" onClick={saveBaasGame}>Save To Baas</button>}
            {games.length === 0 ? <label>Login to Load Masters' Games.<hr />Login: redux Password: redux</label> : ''}
            <ul ref={this.listRef}>
                {hasGames && games.map( (g, idx) => 
                    <li key={idx} className={activeIdx === idx ? 'active flexRow' : 'flexRow'} >
                        <button onClick={() => deleteGame(idx)}>
                            <i className="fa fa-close" />
                        </button>
                        <label onClick={() => loadBaasGame(idx)} >
                            {inDelete && activeIdx === idx ? '... click again to confirm' : g.key}
                        </label>
                    </li>)
                }
            </ul>

            {games.length > 0 && <input type="text" className="gameSearch" 
                    placeholder="Find Game"
                    onChange={this.searchGame}
                    onClick={this.resetSearch}
                ></input>
            }

            <ul className="searchedGames">
                {searchedGames.length > 0 && searchedGames.map( (g, idx) => 
                    <li key={idx} className="flexRow" >
                        <label onClick={() => this.loadSearchBaasGame(idx)} >
                            {g.key}
                        </label>
                    </li>)
                }
            </ul>

        </div>
    }
}

export {BaasList};
