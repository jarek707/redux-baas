import React from "react";

function QuickMenu(props) {
    //let ux = props.ux();

    let toggleBoardSize = () => {
        props.ux().toggleBoardSize();
    }

    return <div className="outterWrap collapse">
        <i id="autoSaveIcon" className="fa fa-download tooltip">
            <div className="tooltiptext up">Toggle Auto/Save</div>
        </i>

        <label id="hideNumbersIcon" className="tooltip">
            <div className="tooltiptext up">Toggle Numbers</div>
            <span>123</span>
        </label>

        <i id="reloadGame" className="fa fa-refresh tooltip">
            <div className="tooltiptext up">To Initial Loaded State</div>
        </i>

        <label id="boardSize" onClick={toggleBoardSize} className="tooltip">
            <div className="tooltiptext up">Toggle Board Size</div>
            <span>19</span>
        </label>
    </div>
}    

export {QuickMenu};
