import React, {useEffect, createRef, useState, useImperativeHandle} from "react";

const CursorIcon = React.forwardRef((props, parentRef) => {
    const cursorIconRef = createRef();
    let cursorIconEl = null;

    const [cursorIconClass, setCursorIconClass] = useState('');

    useEffect( () => {
        cursorIconEl = cursorIconRef.current;
    });

    let setCursorTurn = (inTurn) => {
        if (cursorIconEl) {
            let textEl = cursorIconEl.getElementsByTagName('text')[0];

            if (cursorIconClass === 'Invalid') {
                textEl.innerHTML = 'X';
                textEl.setAttribute('x', 20);
            } else if (cursorIconClass === 'IllegalKO') {
                textEl.innerHTML = 'X-KO';
                textEl.setAttribute('x', 9);
            } else if (cursorIconClass === 'WaitTurn' || !inTurn) {
                textEl.innerHTML = '..wait..';
                textEl.setAttribute('x', 1);
            } else {
                textEl.innerHTML = inTurn;
                textEl.setAttribute('x', inTurn < 10 ? 19 : (inTurn < 100 ? 16 : 12));
            }
        } else {
            console.warn( 'NO cursor ');
        }
    }

    useImperativeHandle( parentRef, () => ({
        hide() {
            cursorIconEl.style.left = -1000;
        },
        setClass(iconClass, inTurn = false) { // turn change
            if (inTurn) {
                let iClass = inTurn%2 === 0 ? 'white' : 'black';
                setCursorIconClass(cursorIconClass => cursorIconClass = iClass)
            } else {
                setCursorIconClass(cursorIconClass => cursorIconClass = iconClass)
            }
            setCursorTurn(inTurn);
        },
        setPos(x, y) {
            cursorIconRef.current.style.top  = y - 34;
            cursorIconRef.current.style.left = x - 24;
        }
    }));

    return (
        <svg height="148" width="48" ref={cursorIconRef}
            className={'mouseCursor ' +  cursorIconClass}
        >
            <circle cx="24" cy="24" r="22" />
            <text y="30"></text>
        </svg>
    )
})

export {CursorIcon};
