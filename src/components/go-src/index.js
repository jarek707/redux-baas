import {List, OrderedMap, Stack} from 'immutable';
import {timer, range, of, from} from 'rxjs';
import {concatMap, map, delay} from 'rxjs/operators';

//import {} from 'rxjs/operators';
//-----------------------------------------------------------------------------------------------
//
//  Desing (a singleton):
//      Classes - 1. UX     - responsible for handling user interactions, inherits from Canvas
//                2. Canvas - all canvas rendering, inherits from Data
//                3. Data   - manages game state
//                          - handles coordinate coversions
//
//
//  Variable/property Naming Conventions:
//
//      coordX, coordY - coordinates (sometimes cX and cY are used inside methods)
//                       example [1, 1] -> [19, 19] on a big board
//
//         posX, posY - canvas coordinates, posX == coordX*this.sqSize
//            x,    y - canvas click position in pixels,
//                      it is immediately normalized to coord/pos
//
//                 LS - localStorage
//
//-----------------------------------------------------------------------------------------------
//'use strict';

let defaultConfig = JSON.stringify(
    {   "sqCount": 13,       // board size (9/13/19)
        "autoSave": true,    // save each move including undo/redo
        "pollValue": 0,
        "hideNumbers": false // display order numbers on the stones
});

const appConfig = {
    hashSpread : 100
}

class Stone {
    constructor(hash, turn, color = 'black') {
        this.color =     color;
        this.live  =      true;
        this.adjacents = [];
        this.turn =      turn;
        this.hash =      hash;
    }

    kill()   { this.live = false }

    get  x()  { return (this.hash - this.hash%appConfig.hashSpread)/appConfig.hashSpread }
    get  y()  { return this.hash%appConfig.hashSpread }
    get  xy() { return [
        (this.hash - this.hash%appConfig.hashSpread)/appConfig.hashSpread,
        this.hash%appConfig.hashSpread]
    }
    get coords() { return {x: this.x, y:this.y} }

    get board() {
        return String.fromCharCode((this.x>9 ? this.x+1 : this.x) + 63) + ':' + (21-this.y)
    };
}

class StoneList {
    constructor() {
        this.initStoneList();
        this.hashSpread = 100;
        this.myTurn = true;
    }

    hashToBoard = hash => {
        let [x, y] = this.unhashA(hash);
        return  String.fromCharCode((x>9 ? x+1 : x) + 63) + ':' + (21-y);
    }

    initStoneList(shadow = []) {
        this.koHash = false;
        this.undone = new Stack();
        this.stones = new OrderedMap({});
        this.turns  = new List([]);
        this.shadow = shadow;
        setTimeout(() => this.updateComponentView(), 200);

        return this;
    }

    adjacents(x, y) {return [[x-1, y], [x+1, y], [x, y-1], [x, y+1]]};

    hashFn   = (x, y = null) => y ? x * this.hashSpread + y : x[0] * this.hashSpread + x[1];

    unhashA = h => [(h - h%this.hashSpread)/this.hashSpread, h%this.hashSpread];

    neighbours( h ) {
        let [x, y] = [(h - h%this.hashSpread)/this.hashSpread, h%this.hashSpread];

        let edgeMin = 2;
        let edgeMax = this.sqCount + 2;
        return [
            x > edgeMin ? this.stones.get(this.hashFn(x - 1, y)) : 'edge',
            x < edgeMax ? this.stones.get(this.hashFn(x + 1, y)) : 'edge',
            y > edgeMin ? this.stones.get(this.hashFn(x, y - 1)) : 'edge',
            y < edgeMax ? this.stones.get(this.hashFn(x, y + 1)) : 'edge'
        ];
    }

    blockWithHash(hash, blockList = []) {
        let stone = this.stones.get(hash);
        if (stone) {
            blockList.push(stone);

            stone.adjacents.forEach( s => {
                blockList.find(st => st.hash === s.hash) || this.blockWithHash(s.hash, blockList)
            });
        }

        return blockList || [];
    }

    // TODO: the whole block is not checked with 4 neighbours away from edge
    isCaptured(block, fromSelf = false) {
        return block.map( s => {
            return this.neighbours(s.hash).map( n => {
                return (typeof n !== 'undefined' && n.live) || (n === 'edge')
            }).every(i => i)
        }).every(i => i);
    }

    isKO( hash, clearedHash ) {
        let myColor = this.stones.get(hash).color;

        let koCount = this.neighbours(hash)
            .filter(s => s && s.color !== myColor && s.hash !== clearedHash)
            .length;

        return koCount === 3 ? clearedHash : false;
    }

    clearCaptured(hash) {
        let hasCaptured = false;
        let oppositeAdjacents = this.oppositeAdjacents(hash);
        let capturedBlocks = [];
        oppositeAdjacents.forEach( s => {
            let block = this.blockWithHash(s.hash);
            let captured = this.isCaptured( block )
            if (captured) {
                block.forEach( s => s.kill() );
                capturedBlocks.push( block );
            }
            hasCaptured |= captured;
        });

        if (capturedBlocks.flat().length === 1) {
            this.koHash = this.isKO(hash, capturedBlocks.flat()[0].hash);
        }
        return hasCaptured;
    }

    amICaptured(stone) {
        let ret = false;
        let neighbours = this.neighbours( stone.hash )
                        .filter( n => n && (n.live || n === 'edge') );

        if (neighbours.length === 4) {
            let brothers = neighbours.filter(n => n && n.color === stone.color);
            ret = !brothers.map(b => {
                let ret = this.isCaptured(this.blockWithHash(b.hash), true)
                return ret;
            })
            .some(x => x === false);
        }

        return ret;
    }

    oppositeAdjacents(hash) {
        let stone = this.stones.get(hash);
        if (stone) {
            let oppositeColor = stone.color === 'black' ? 'white' : 'black';

            return  this.neighbours(hash)
                    .map(n => n && n.color === oppositeColor && n)
                    .filter(n => n) || [];
        } else {
            return [];
        }
    }

    setAdjacents(myHash, color) {
        this.neighbours(myHash)
        .filter( p => p && p.color === color )
        .forEach(p => {
            let stone     = this.stones.get(myHash);
            let neighbour = this.stones.get(p.hash);
            stone.adjacents.push( neighbour );
            neighbour.adjacents.push( stone );
        });
    }

    removeLastStone(hash) {
        let lastTurn = this.turns.last();

        if (lastTurn) {
            let hash = lastTurn.hash;

            this.neighbours( hash )
            .filter( s => s && s.adjacents )
            .forEach( s => {
                let idx = s.adjacents.findIndex( a => a.hash === hash )
                idx === -1 || s.adjacents.splice(idx, 1);
            });

            this.stones = this.stones.delete(hash);
            this.turns  = this.turns.pop();
        }
    }

    startLive() { this.inLive = true;  }
    exitLive()  { 
        this.inLive = false; 
        this.clearGame();
        this.myTurn = true;
    }

    myMove(myMove = true) { 
        this.myTurn = myMove;
        this.callComponent( {action: 'WaitTurn', myMove: myMove} );
    }

    addLiveStone(hash) {
        this.inLive = true;
        this.addStone( ...this.unhashA( hash ), true );
    }

    addStone(x, y, noLive = false) {
        let myHash   = this.hashFn(x, y);
        if (this.inLive && !this.myTurn) {
            this.callComponent( {action: 'WaitTurn'} );
            return null;
        }

        if (this.koHash === myHash) {
            this.callComponent( {action: 'IllegalKO'} );
            return null;
        } else {
            this.koHash = false;
        }

        let wasUsed =  this.stones.get(myHash);
        if (wasUsed && wasUsed.live) {
            this.undone.clear(); // we are putting new stone where one was undone, clear undone stack
            return null;
        }

        let color = this.turns.size ? (this.turns.last().color === 'black' ? 'white' : 'black') : 'black';

        let newStone = new Stone(myHash, this.turns.size, color);
        this.stones  = this.stones.set(myHash, newStone);
        this.turns   = this.turns.push(this.stones.get(myHash));

        this.setAdjacents(myHash, color);

        let hasCaptured = this.clearCaptured( myHash );
        let iAmCaptured = this.amICaptured( newStone );

        if (iAmCaptured && !hasCaptured) {
            this.removeLastStone();
            this.callComponent( {action: 'Invalid'} );
            console.log( 'I AM Captured', newStone.board, newStone.turn);
            return null;
        }

        noLive || this.callComponent( {action: 'LiveMove', turns: this.turns} );

        hasCaptured && setTimeout(() => this.replay(), 0);

        return newStone;
    }

    stonesToShadow() {
        this.shadow = [];
        this.turns.forEach( t => this.shadow.push(t.hash) );
    }

    restoreFromShadow(shadow) {
        if (typeof shadow === 'undefined') {
            shadow = this.shadow || [];
        }

        this.initStoneList(shadow);

        shadow.forEach( s => this.addStone(...this.unhashA(s)) );
        this.updateComponentView();
    }

    updateComponentView() {
        if (typeof this.callback === 'function') {
            let payload = {color: 'black', action: 'ButtonChanged', nextTurn: 1};

            let newStone = this.turns.last();
            if (newStone) {
                payload = {
                            color: newStone.color === 'black' ? 'white' : 'black',
                            action: 'ButtonChanged',
                            nextTurn: newStone.turn + 2
                          };
            }

            this.callback( payload );
            this.updateButtons();
        }
    }
}

//
//  class Data
//
class Data extends StoneList {
    constructor() {
        super();
        this.config  = {};

        this.getConfigFromLS();
        this.gameStateFromStore();
    }

    //
    // gameStateFromStore - load game state from localStorage
    //
    gameStateFromStore() {
        let shadow = JSON.parse(localStorage.shadow || '[]');
        // HACK - this should be exectued once the UX instnce finished initialization
        timer(0).subscribe( () => 
        {
            this.restoreFromShadow( shadow || [] );
            let masterGame = localStorage.getItem('masterGame');
            let meta = JSON.parse(masterGame) || [];
            meta && meta.length && (this.dropArea.value = this.buildGameMeta( meta )[0]);
        });
    }

    //
    // gameStateToStore - save game state in localStorage
    //
    gameStateToStore( shadow ) {
        if (this.config.autoSave) {
            if (shadow === 'clear') {
                shadow = '';
                localStorage.setItem('masterGame', '[]');
            }
            localStorage.setItem('shadow', JSON.stringify(shadow));
        }
    }

    saveState() {
        let shadow = this.turns.map( t => t.hash ).toJS();
        this.gameStateToStore( shadow );
    }

    saveConfigToLS() {
        localStorage.setItem('config', JSON.stringify(this.config));
    }

    getConfigFromLS() {
        this.config = JSON.parse(localStorage.config || defaultConfig);
    }

    clearGame() {
        this.initStoneList();
        this.gameStateToStore('clear');
        this.updateComponentView();
    }

}
//
// Class Canvas - manage all canvas rendering using two canvas DOM elments
//
//  this.grid - lower canvas displaying grid lines and orientation dots/circles
//  this.main - upper canvas displaying stones
//
class Canvas extends Data {
    constructor() {
        super();
        this.circlePos = []; // array of orientational dots/circles (x, y - coords)

        this.main    = document.getElementById("mainCanvas");
        this.grid    = document.getElementById("gridCanvas");
        this.mainCtx = this.main.getContext("2d");
        this.gridCtx = this.grid.getContext("2d");

        this.letters = ['A','B','C','D','E','F','G','H','J','K','L','M','N','O','P','Q','R','S','T'];

        // TODO - fix this WORKAROUND - wait for all dom elements and data on startup
        window.onresize = evt => this.handleWindowResize();
    }

    handleWindowResize() {
        if (!this.grid) return;// TODO - HACK - properly Integrate with React

        let bodyWidth = document.body.clientWidth;

        let offset = {w: bodyWidth > 540 ? 142 : 0, h: 186};
        this.gridWidth =  Math.min( bodyWidth - offset.w, document.body.clientHeight - offset.h);

        this.gridWidth = Math.max(300 * this.sqCount/9, this.gridWidth );
        this.sqSize = this.gridWidth / (this.sqCount + 2);

        this.grid.width  =
        this.main.width  =
        this.grid.height =
        this.main.height = this.gridWidth;
        this.initGrids();

        this.replay();
    }

    //
    // grid canvas - START
    //
    initGrids() {

        this.circlePos = [];

        this.main = document.getElementById("mainCanvas");
        this.grid = document.getElementById("gridCanvas");

        if (!this.grid) return ; // TODO - HACK - properly Integrate with React

        this.mainCtx = this.main.getContext("2d");
        this.gridCtx = this.grid.getContext("2d");

        this.main.onclick = (evt) => this.goClicked( evt );

        let pos = this.sqSize * 3;
        let fillSpace = this.sqCount/3;

        //
        // bulid orientational circles/dots
        //
        if (this.sqCount === 18) {
            for (let x = 0; x<3; x++)
                for (let y = 0; y<3; y++)
                    this.circlePos.push([pos + x*(this.sqSize*fillSpace), pos + y*(this.sqSize*fillSpace)]);
        } else if (this.sqCount === 12) {
            for (let x = 0; x<3; x++)
                for (let y = 0; y<3; y++) {
                    fillSpace = this.sqCount/4;
                    if ((x-y)%2 === 0) // render only corners and center
                        this.circlePos.push([pos + x*(this.sqSize*fillSpace), pos + y*(this.sqSize*fillSpace)]);
                }
        } else {
            pos = this.sqSize * 2;
            for (let x = 0; x<3; x++)
                for (let y = 0; y<3; y++) {
                    fillSpace = this.sqCount/4;
                    if ((x-y)%2 === 0) // render only corners and center
                        this.circlePos.push([pos + x*(this.sqSize*fillSpace), pos + y*(this.sqSize*fillSpace)]);
                }
        }

        this.renderBoard();
        //this.replay(true/* fast */); // TODO - Refactor - need to call this from Game object also this should not be in initGrid
    }

    //
    // resizeBoard - sets the number of grid lines and re-renders
    //
    //  sqCount - number of lines (square count)
    //
    resizeBoard(sqCount) {
        if (typeof sqCount != 'undefined') {
            this.config.sqCount = sqCount;
        }

        this.sqCount = parseInt(this.config.sqCount) - 1;
        this.handleWindowResize();
        this.saveConfigToLS();
        this.updateComponentView();
    }

    toggleBoardSize() {
        switch (this.sqCount) {
            case 18: this.resizeBoard(13); break;
            case 12: this.resizeBoard( 9); break;
            case 8:  this.resizeBoard(19); break;
            default: break;
        }
    }

    //
    // setGridSpaces - sets the size of the squares and renders grid lines
    //
    //  sqSize - size of each square in pixels
    //
    setGridSpaces(sqSize) {
        this.initGrids();
        this.saveConfigToLS();
    }

    renderBoard() { // render Vertical and Horizontal grid lines with orientation dots on the Canvas
        let halfSquare = this.sqSize/2
        halfSquare = this.sqSize;

        this.gridCtx.strokeStyle = '#777';
        // +1 is needed when resizing the browser
        this.gridCtx.clearRect(0, 0, this.grid.width, this.grid.height);
        this.gridCtx.lineWidth = 1;

        // Grid Lines
        for (let i=0; i<this.sqCount + 1; i++) {
            // canvas weirdness - need to use exact integers and start at halfPixel to get 1px lines
            let startPos = parseInt(halfSquare + this.sqSize * i) + 0.5;
            let endPos   = parseInt(halfSquare + this.sqSize * this.sqCount) + 0.5;

            // Vertical
            this.gridCtx.moveTo(startPos, halfSquare);
            this.gridCtx.lineTo(startPos, endPos);

            // Horizontal
            this.gridCtx.moveTo(halfSquare, startPos);
            this.gridCtx.lineTo(endPos,     startPos);
            this.gridCtx.stroke();
        }

        // Orienation dots
        for (let i=0; i<this.circlePos.length; i++) {
            this.gridCtx.beginPath();

            let [circX, circY] = this.circlePos[i];
            this.gridCtx.arc(circX + halfSquare, circY + halfSquare, 5, 0, 2 * Math.PI);
            this.gridCtx.fillStyle = 'black';
            this.gridCtx.fill();
        }

        let fontSize = 14;
        if (this.sqCount === 8) {
            fontSize = 20;
        } else if (this.sqCount === 12) {
            fontSize = 18;
        }

        fontSize = 18;

        this.gridCtx.font = `${fontSize}px Assistant`;
        for (let i=0; i<this.sqCount + 1; i++) {
            let numOff =
                this.centerText(0, i+1, this.mainCtx.measureText((i+1).toString()).width, fontSize);
            let letterOff =
                this.centerText(i+1, 0, this.mainCtx.measureText(this.letters[i]).width, fontSize);

            this.gridCtx.fillStyle = '#777';
            this.gridCtx.fillText(this.letters[i],  letterOff.x - 1, letterOff.y - 4);
            this.gridCtx.fillText((this.sqCount - i+1).toString(), numOff.x + this.sqSize/4 + 2, numOff.y - this.sqSize/3 - 2);
        }

        this.gridCtx.stroke();
    }

    centerText(xI, yI, width, fontSize, toRight = false) {
        let baseX = xI * this.sqSize;
        let baseY = yI * this.sqSize;
        return {x: baseX - width/2, y: baseY + fontSize/2 + this.sqSize/3};

    }

    //
    // renderCanvasStone - renders a stone on the canvas
    //
    //  stone - stone instance
    //
    renderCanvasStone(stone) {
        let {color, turn}    = stone;
        let [coordX, coordY] = stone.xy;

        let xPos = coordX * this.sqSize;
        let yPos = coordY * this.sqSize;

        let maxPosVal =  this.sqSize * (this.sqCount + 2);
        // Don't render if the stone comes from a bigger game board or is not live
        if (xPos <= maxPosVal && yPos <= maxPosVal && stone.live) {
            let lastTurn       = this.turns.last().turn     === turn;
            let secondLastTurn = this.turns.last().turn - 1 === turn;

            let size = this.sqSize;
            let halfSquare = size;
            let radius = halfSquare/2 - 2;

            this.mainCtx.beginPath();
            this.mainCtx.arc(
                xPos - halfSquare, yPos - halfSquare,
                radius, 0, 2 * Math.PI, true
            );
            let endRadius = radius * (color === 'black' ? 0.2 : 0.9);

            let grd = this.mainCtx.createRadialGradient(
                xPos - halfSquare,      yPos - halfSquare, radius,
                xPos - halfSquare - 50, yPos + halfSquare, endRadius
            );

            if (color === 'black') {
                grd.addColorStop(0, lastTurn || secondLastTurn ? 'black' : '#222');// : 'black');
                grd.addColorStop(1, '#aaa');
            } else {
                grd.addColorStop(0, '#bbb');
                grd.addColorStop(1, lastTurn || secondLastTurn ? '#ddd' : 'white');
            }

            Object.assign(this.mainCtx, {   shadowColor   : '#555',
                                            shadowBlur    :  1,
                                            shadowOffsetX :  1,
                                            shadowOffsetY : -1,
                                            fillStyle     : grd,
                                            strokeStyle : 'transparent'});
            this.mainCtx.fill();

            let fontSizeRatio = halfSquare > 20 ? 3 : 2.3;
            let fontSize = parseInt(size/fontSizeRatio);

            let textWidth = this.mainCtx.measureText(turn + 1).width;

            halfSquare = halfSquare/2;
            let offX = halfSquare + textWidth/2;
            let offY = halfSquare/2 + fontSize/(6 - fontSizeRatio);

            xPos -= halfSquare;
            yPos -= halfSquare;

            Object.assign( this.mainCtx, {
                shadowBlur:     0,
                shadowOffsetX : 0,
                shadowOffsetY : 0,
                font: `${lastTurn || secondLastTurn ? 'bold' : ''} ${fontSize}px Assistant`,
                fillStyle : lastTurn ? 'red' : (secondLastTurn ? 'yellow' : '#777')
            });

            if (!this.config.hideNumbers) {
                this.mainCtx.fillText(turn + 1, xPos - offX, yPos - offY - 1);
            }
        }
    }

    //
    // grid canvas - END
    //

    //
    // main canvas - START
    //
    clearGame(andData = false) {
        this.mainCtx.clearRect(0, 0, this.grid.width, this.grid.height);
        andData && super.clearGame();
    }

    //
    // main canvas - END
    //
}

//
// Class UX - manages DOM elements
//
class UX extends Canvas {
    constructor() {
        super();
        this.color = 'black';
        let domIds = {
            single:         ["replay",    "autoSave", "toStart", "clearGame", "hideNumbers"],
            undo:           ["undoLast", "redoLast",  "forward5",    "backward5"],
            notButtons:     [ "dropArea", "canvases", "boardSize",
                                "autoSaveIcon", "hideNumbersIcon",
                                "toStartButtonIcon", "replayButtonIcon",
                                "reloadGame"
                            ]
        }

        for (let i in domIds) {
            for (let j in domIds[i]) {
                let button = i === 'notButtons' ? '' : 'Button';
                this[domIds[i][j] + button] = document.getElementById(domIds[i][j]);
            }
        }

        this.replayButtonIcon.onclick = () => {
            this.toEnd(false);
        }

        document.getElementById('autoSaveIcon').style.opacity = this.config.autoSave ? 1 : 0.3;

        this.autoSaveIcon.onclick   = this.toggleAutoSave;
        this.toStartButtonIcon.onclick = () => this.toStart();

        this.undoLastButton.onclick  = () => this.undoLast();
        this.redoLastButton.onclick  = () => this.redoLast();
        this.forward5Button.onclick  = () => this.redoLast(5);
        this.backward5Button.onclick = () => this.undoLast(5);

        this.dropArea.onpaste = this.parseGame;
        this.dropArea.onclick = this.prepareForPaste;
        this.dropArea.ondrop  = this.handleGameDrop;
        this.canvases.ondragover = evt => { evt.preventDefault(); return false; }
        this.canvases.ondrop  = this.handleGameDrop;

        //this.hideNumbersIcon.onclick = () => this.toggleNumbers();
        this.reloadGame.onclick      = () => this.restoreFromShadow(this.shadow);

        this.resizeBoard();
        this.grid && this.setGridSpaces();
        this.toggleNumbers(this.config.hideNumbers);

        this.updateButtons();
        this.rawGame = null;
    }

    toggleAutoSave = () => {
        this.config.autoSave = !this.config.autoSave;
        this.updateButtons();
        this.saveConfigToLS(true /*force LS write*/);
        document.getElementById('autoSaveIcon').style.opacity = this.config.autoSave ? 1 : 0.3;
    }

    registerBaas(cb) {
        this.callback = cb;
    }

    callComponent( data ) {
        //console.log( typeof this.callback, data, 'in Call Comp');
        typeof this.callback === 'function' && this.callback( data );
    }


    handleGameDrop = evt => {
        evt.stopPropagation();
        evt.preventDefault();

        let reader = new FileReader();

        reader.onload = file =>
            this.loadPastedGame( file.target.result );

        this.dropFileName = evt.dataTransfer.files[0].name;
        reader.readAsText( evt.dataTransfer.files[0] );
    }

    prepareForPaste = (gameDataEvt) => {
        gameDataEvt.target.value = '';
        this.clearGame();
    }

    buildGameMeta( data ) {
        let meta = data[1].match(/([A-Z]{2})(\[.*?\])?/g)
                    .map(s => s.split('['))
                    .reduce((agg, s) => {
                        agg[s[0]] = s[1] ? s[1].replace(/\]/,'') : '';
                        return agg;
                    }, {});

        let {EV, PB, RE, PW, RO} = meta;
        let [winner, score] = RE.split('+');
        EV = EV ? EV : 'Unknown Event';
        RO = RO ? RO : '';

        let win = winner === 'B' ? [`(${score})`, ''] :['' ,`(${score})`];

        return [`${EV.substring(0,20)}\nB:${win[0]} ${PB}\nW:${win[1]} ${PW}\n${RO}`, meta] ;
    }

    loadPastedGame = data => {
        data =  data.replace(/(\n|\r|\))/g,'').split(';');
        this.inLive = false;

        if (data.length) {
            let used = data.filter( i =>
                i.substring(0,1) === 'B' || i.substring(0,1) === 'W'
            )
            .map( i => [i.charCodeAt(2) - 95, i.charCodeAt(3) - 95] );

            this.shadow = [];

            from(used).subscribe(s => this.shadow.push( this.hashFn(...s) ) );

            this.restoreFromShadow( this.shadow );
            this.gameStateToStore(this.shadow);

            if (data.length > 1) {
                let [textAreaContent, meta] = this.buildGameMeta(data);

                this.dropArea.value = textAreaContent;
                this.callComponent( meta );
                this.updateComponentView();
                localStorage.setItem('masterGame', JSON.stringify(data));
            } else {
                this.dropArea.value = used.map( d => [d.color, d.coordX, d.coordY].join(':')).join('\n');
            }
        } else {
            this.dropArea.value = 'Invalid Data Format';
        }
    }

    parseGame = (gameDataEvt) => {
        setTimeout( () => {
            this.loadPastedGame( gameDataEvt.target.value );
        }, 10);
    }

    //
    // toggleNumbers - controls display of stone numbers,
    //                 each number corresponds to the order in which it was placed
    //
    toggleNumbers(hideNumbers) {
        if (typeof hideNumbers != 'undefined')
            this.config.hideNumbers = !hideNumbers;

        if (this.config.hideNumbers) {
            this.config.hideNumbers = false;
        } else {
            this.config.hideNumbers = true;
        }

        //this.hideNumbersIcon.style.opacity = this.config.hideNumbers ? 0.3 : 1;

        this.initGrids();
        this.saveConfigToLS();
        this.replay();
    }

    //
    // updateButtons - shows/hide undo and redo buttons and sets opacity of the autoSave button
    //
    updateButtons() {
        this.redoLastButton.style.opacity = this.undone.size ? '1' : '0.3';

        this.backward5Button.style.opacity = this.turns.size > 4 ? '1' : '0.3';
        this.forward5Button.style.opacity  = this.undone.size > 4 ? '1' : '0.3';

        this.boardSize.getElementsByTagName('span')[0].innerText = this.sqCount + 1;
    }

    clearGame() {
        this.dropArea.value = '';

        super.clearGame(true/*clear game data*/);
        this.updateComponentView();
    }

    toStart() {
        this.undoLast(this.turns.size, 0);
        this.updateComponentView();
        this.replay();
    }

    toEnd () {
        this.redoLast(this.undone.size, 0);
    }

    //
    //  REPLAY
    //  replay - clears stones from the board and re-renders them sequentially
    //
    //      slow - slow rendering - 300ms between each stone
    //      fast - all stones are rendered immediately (overwritten by slow if set)
    //
    //      default - rendering is fast but there is a 30ms delay between each stone
    //

    replay(speed = 0) {
        this.mainCtx.clearRect(0, 0, this.grid.width, this.grid.height);

        // HACK
        this.stones = this.stones.sort( (a,b) => a.turn > b.turn ? 1 : -1 );

        if (speed !== 303)
        from(this.stones.entries())
        .pipe(
            map(e => e[1]),
            //concatMap(x => of(x).pipe(delay(speed)))
        )
        .subscribe( s =>
            s.live && this.renderCanvasStone(s)
        );
    }

    //
    // undoLast - Undo Last 'howMany' moves
    //            Recursively removes last 'n' stones with 100ms delay between each
    //
    undoLast(count = 1, speed = 100) {
        range(0, count)
        .pipe(
            concatMap(x => of(x).pipe(delay(speed)))
        )
        .subscribe( () => {
            if (this.turns.size > 0) {
                let lastTurn = this.turns.last();

                // restore captured blocks
                let opAdjacents = this.oppositeAdjacents(lastTurn.hash);
                opAdjacents.forEach( a => this.blockWithHash(a.hash).forEach( s => s.live = true ) );

                this.undone  = this.undone.push( [lastTurn.x, lastTurn.y] );
                this.removeLastStone();

                this.replay();
                this.updateComponentView();
            }
        });
    }

    //
    // redoLast - same as above, except the other direction
    //
    redoLast(count = 1, speed = 100) {
        range(0, count)
        .pipe(
            concatMap(x => of(x).pipe(delay(speed)))
        )
        .subscribe( () => {
            if (this.undone.size > 0) {
                this.addStone( ...this.undone.peek() );
                this.undone = this.undone.pop();

                this.replay();
                this.updateComponentView();
            }
        });
    }

    stonePlaceSound() {
        var audio = new Audio('./assets/tick.mp3');
        audio && audio.play();
    }

    //
    // handleBoardClick - places or removes a stone from the board
    //
    //  mouseX, mouseY - x and y of the mouse click
    //
    handleBoardClick(mouseX, mouseY) {
        // Convert mouse coordinates to board x/y position (int, int)
        let [x, y] = [  Math.round((mouseX + this.sqSize)/this.sqSize),
                        Math.round((mouseY + this.sqSize)/this.sqSize) ];

        let myHash = this.hashFn(x, y);

        if (x > 1 && y > 1 && x < this.sqCount + 3 && y < this.sqCount + 3) {
            let stonePos   = this.stones.get(myHash);
            let spaceTaken = stonePos && stonePos.live;

            if (!spaceTaken && this.addStone(x, y) ) {
                this.renderCanvasStone(this.turns.last());
                this.turns.size > 1 && this.renderCanvasStone(this.turns.pop().last());
                this.turns.size > 2 && this.renderCanvasStone(this.turns.pop().pop().last());

                this.stonePlaceSound();
                this.inLive || this.updateComponentView();
            }
        }
    }

    //
    // goClicked - handle mouse click on the go board
    //
    //       evt - mouse click event
    //
    goClicked( evt ) {
        this.handleBoardClick(evt.offsetX, evt.offsetY);
    }
}

export {UX};
