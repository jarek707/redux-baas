import React from "react";
import {ReduxNavLink, ExternalLink} from './hoc';

const gitLabUrl = "https://gitlab.com/jarek707/redux-baas/blob/master/src/";

export default () =>
    <div className="info">
        <div>
            <h3>React App with Persistent Redux.</h3>
            <h4>
                It features persistent redux store extension responsible for writing store data to database using &nbsp;
                <span className="nowrap">
                    <ExternalLink href="https://simplebaas.com">
                        simpleBaas (Backend As A Service)
                    </ExternalLink>.
                </span>
            </h4>

            <h4>
                Log in and add/remove items to/from Basket.<br />
                After the page reload the state should be preserved.
            </h4>
            <span>
                Go to <ReduxNavLink to="/redux-baas"
                >redux-baas</ReduxNavLink> page or view code: &nbsp;
                    <code>
                        <ExternalLink href={gitLabUrl + 'redux-baas/index.js'}>
                            index.js 
                        </ExternalLink>
                        ,&nbsp;
                        <ExternalLink href={gitLabUrl + 'redux-baas/reduxBaas.js'}>
                            reduxBaas.js
                        </ExternalLink>
                </code>
            </span>
        </div>

        <div>
            <h3>Game of GO!</h3>
            <h4>Vanilla Javascript code wrapped inside a React component.
            </h4>
            <span>
                Go to <ReduxNavLink to="/go"
                >GO!</ReduxNavLink> page. &nbsp;
                <code>
                    <ExternalLink href={gitLabUrl + 'components/go-src/index.js'}>
                    view code
                    </ExternalLink>
                    &nbsp;
                    <ExternalLink href={gitLabUrl + 'components/Go.js'}>
                    view component code
                    </ExternalLink>
                </code>
            </span>
        </div>

        <div>
            <h3>Simple Timer For GO!</h3>
            <h4>Small component using animationFrame and component tear-down
            </h4>
            <span>
                Go to <ReduxNavLink to="/timer"
                >Timer</ReduxNavLink> page. &nbsp;
                <code>
                    <ExternalLink href={gitLabUrl + 'components/Timer.js'}>
                    view code
                    </ExternalLink>
                </code>
            </span>
        </div>

        <div>
            <span>
                Please note - all of the above modules are currently under development.
            </span>
        </div>
    </div>
