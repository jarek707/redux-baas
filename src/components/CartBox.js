import React, {useState} from "react";
import {store} from '../redux-baas';

export default (props) => {
    const [boxData] = useState(props.storeContent);

    return  <div className="box"
                onClick={() => store.dispatch({type: 'REMOVE_FROM_CART', payload:  boxData})}
            >
                <img src={boxData.img} alt="StoreItem"></img>
                <h2>{boxData.title}</h2>
                <p>
                    {boxData.qty} - {boxData.price * boxData.qty}
                </p>
            </div>
}
