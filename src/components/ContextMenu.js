import React, {useEffect, createRef, useImperativeHandle} from "react";

const ContextMenu = React.forwardRef((props, parentRef) => {
    let contextMenuRef = createRef();
    let ctxEl = null;

    let menuClick = (selected, evt) => {
        switch (selected) {
            case 'Undo'     : props.ux().undoLast();       break;
            case 'Undo5'    : props.ux().undoLast(5, 100); break;
            case 'Redo'     : props.ux().redoLast();       break;
            case 'Redo5'    : props.ux().redoLast(5, 400); break;

            case 'toStart'  : props.ux().toStart();        break;
            case 'toEnd'    : props.ux().toEnd();          break;

            case 'Clear'    : props.ux().clearGame();      break;
            case 'AutoSave' : props.ux().toggleAutoSave(); break;
            case 'SaveState': props.ux().saveState();      break;
            case '19'       : props.ux().resizeBoard(19);  break;
            case '13'       : props.ux().resizeBoard(13);  break;
            case '9'        : props.ux().resizeBoard(9);   break;
            default:                                    break;
        }
    }

    useEffect( () => {
        // TODO: figure out scoping here ctxEl isn't always available
        ctxEl = contextMenuRef.current;
    });

    useImperativeHandle( parentRef, () => ({
        hide() { contextMenuRef.current.style.left = '-1000px' },
        show(evt) {
            evt.preventDefault();
            evt.stopPropagation();

            let {clientX, clientY} = evt;
            let bodyEl = document.body;

            let offsetX = Math.min(0, bodyEl.clientWidth  - ctxEl.clientWidth  - clientX);
            let offsetY = Math.min(0, bodyEl.clientHeight - ctxEl.clientHeight - clientY);

            ctxEl.style.top  = (clientY + offsetY - 20) + 'px';
            ctxEl.style.left = (clientX + offsetX - 120) + 'px';
        }
    }));

    return (<div className="contextMenu" ref={contextMenuRef}>
        <ul>
            <li onClick={() => menuClick('Undo')}>
                <i className="fa fa-undo"></i>Undo 
            </li>
            <li onClick={() => menuClick('Redo')}>
                <i className="fa fa-redo"></i>Redo 
            </li>
            <li className="flex3">
                <button>
                <i className="fa fa-fast-backward"
                    onClick={() => menuClick('toStart')}
                ></i>
                </button>
                <button>
                <i className="fa fa-angle-double-left"
                    onClick={() => menuClick('Undo5')}
                ></i>
                </button>
                <button>
                <i className="fa fa-angle-double-right"
                    onClick={() => menuClick('Redo5')}
                ></i>
                </button>
                <button>
                <i className="fa fa-fast-forward"
                    onClick={() => menuClick('toEnd')}
                ></i>
                </button>
            </li>

            <li onClick={() => menuClick('Clear')}>
                <i className="fa fa-bomb"></i>Clear Game
            </li>

            <li onClick={() => menuClick('AutoSave')}>
                <i className="fa fa-download"></i>Auto Save 
            </li>
            <li className="flex3">
                <button onClick={() => menuClick('19')}>19</button>
                <button onClick={() => menuClick('13')}>13</button>
                <button onClick={() => menuClick('9')} >9</button>
            </li>
            <li onClick={() => menuClick('SaveState')}>
                <i className="fa fa-download"></i>Save State
            </li>
        </ul>
    </div>)
});

export {ContextMenu};
