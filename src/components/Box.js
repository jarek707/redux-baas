import React, { Component } from "react";
import {store} from '../redux-baas';

export default class Box extends Component {
    constructor(props) {
        super(props);
    }

    itemData(key) {
        return key ? this.props.storeContent[key] : this.props.storeContent;
    }

    handleClick = () =>
        store.dispatch({type: 'ADD_TO_CART', payload: {...this.itemData(), qty: '1'}})

    render() {
        return (
            <div className="box"
                onClick={ () => this.handleClick() }
            >
                <img src={this.itemData('img')} alt="StoreItem"></img>
                <h2>{this.itemData('title')}</h2>
                <p>
                    <span>
                        {this.itemData('qty')} - {this.itemData('price').toFixed(2)}
                    </span>
                </p>
            </div>
        )
    }
}
