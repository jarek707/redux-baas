import React, { Component } from "react";
import {NavLink} from "react-router-dom";

export default class Samples extends Component {
    render() {
        return (
            <div className="info">
                <div>
                    <h3>React App with Persistent Redux.</h3>
                    <h4>
                        It features persistent redux store extension responsible for writing store data to database using &nbsp;
                        <span className="nowrap">
                            <a 
                                rel="noopener noreferrer"
                                target="_blank" 
                                href="https://simplebaas.com">
                                simpleBaas (Backend As A Service)
                            </a>.
                        </span>
                    </h4>

                    <h4>
                        Log in and add/remove items to/from Basket.<br />After the page reload the state should be preserved.
                    </h4>
                    <span>
                        Go to <NavLink to="/redux-baas">redux-baas</NavLink> page or view code: &nbsp;
                            <code>
                                <a 
                                    rel="noopener noreferrer"
                                    target="_blank" 
                                    href="https://gitlab.com/jarek707/redux-baas/blob/master/src/redux-baas/index.js">
                                    index.js 
                                </a>
                                ,&nbsp;
                                <a 
                                    rel="noopener noreferrer"
                                    target="_blank" 
                                    href="https://gitlab.com/jarek707/redux-baas/blob/master/src/redux-baas/reduxBaas.js">
                                    reduxBaas.js
                                </a>
                        </code>
                    </span>


                </div>

                <div>
                    <h3>Game of GO!</h3>
                    <h4>Originally written in AngularJs 1.7.2, wrapped inside a React component.
                    </h4>
                    <span>
                        Go to <NavLink to="/go">GO!</NavLink> page. &nbsp;
                            <code>
                                <a 
                                    rel="noopener noreferrer"
                                    target="_blank" 
                                    href="https://gitlab.com/jarek707/redux-baas/blob/master/src/components/go-src/index.js"
                                >
                                    view code
                                </a>
                        </code>
                    </span>
                </div>

                <div>
                    <span>
                        Please note - both of the above modules are currently under development.
                    </span>
                </div>

            </div>
        )
    }
}
