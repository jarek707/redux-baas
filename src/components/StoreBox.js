import React, { Component } from "react";
import {store} from '../redux-baas';

export default class StoreBox extends Component {
    itemData(key) { // getter for Box data and its items
        return key ? this.props.storeContent[key] : this.props.storeContent;
    }

    render() {
        return  <div className="box"
                    onClick={() => store.dispatch({type: 'ADD_TO_CART', payload: {...this.itemData(), qty: '1'}})}
                >
                    <img src={this.itemData('img')} alt="StoreItem"></img>
                    <h2>{this.itemData('title')}</h2>
                    <p>
                        {this.itemData('qty')} - {this.itemData('price').toFixed(2)}
                    </p>
                </div>
    }
}
