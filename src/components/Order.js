import React, {Component} from "react";
import StoreBox from './StoreBox';
import CartBox from './CartBox';
import {store} from '../redux-baas';
import {reduxBaas} from '../redux-baas/reduxBaas';

export default class Order extends Component {
    constructor(props) {
        super(props);
        this.total = 0;
        this.state = {
            doSpin: false
        }
        this.refresh = this.refresh.bind(this);
    }

    getTotal() {
        let total = this.props.basket.reduce( (agg, i) => {
            agg += i.price * i.qty;
            return agg;
        }, 0);

        return total ? `($${total.toFixed(2)})` : '(empty)';
    }

    refresh() { // reload baas data to redux store
        this.setState({doSpin: true});

        reduxBaas.refresh('basket')
        .then(  () => this.setState({doSpin: false}) )
        .catch( () => this.setState({doSpin: false}) );
    }

    render() {
        if (store.getState().login.loggedIn) {
            return (
                <div>
                    <div className="twoCols headings">
                        <div id="store">
                            Inventory <span>(click on an item  to put it in the basket)</span>
                        </div>
                        <div id="cart">
                            Basket {this.getTotal()}<span>(click on an item to remove from the basket)</span>
                            <i className={`fa fa-refresh + ${this.state.doSpin ? 'fa-spin' : ''}`}
                                onClick={this.refresh}
                            ></i>
                        </div>
                    </div>

                    <div className="twoCols">
                        <div id="store">
                            {this.props.shop.map( i =>
                                i.qty ? <StoreBox key={i.id} storeContent={i} source="store" /> : ''
                            )}
                        </div>
                        <div id="cart">
                            {this.props.basket.map( i =>
                                <CartBox key={i.id} storeContent={i} source="cart" />
                            )}
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="beforeLogin">
                    <br /><br /><br />
                    <div>
                        Please log in (Email/Login: <span>redux</span>, Password: <span>redux</span>)
                    </div>
                </div>
            )
        }
    }
}
