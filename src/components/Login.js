import React, {Component, useEffect, useState} from "react";
import { Button, FormGroup, FormControl } from "react-bootstrap";
import "./style/Login.less";
import {login, logout, heartbeat, refreshFromBaas} from '../redux-baas';
import {ReduxNavLink} from './hoc';

const HeaderWrap = ({children, props, loginCount}) => {
    const [count, setCount] = useState(0);

    useEffect( () => {
        setCount(count + 1);
    }, [loginCount]);

    return  <header className="App-header">
                <div className="home">
                    <ReduxNavLink to="/">
                        <i className="fa fa-home"></i>
                        <div className="myName">Jarosław Kulikowski</div>
                    </ReduxNavLink>
                </div>

                <div className="Login">
                    {children}
                </div>
            </header>
}

export default class Login extends Component {
    constructor(props) {
        super(props);

        // TODO; remove all references to this.state and switch to redux login state
        this.state = { email: "", loggedIn: false, password: "" };
        this.loginCount = 0;

        this.props.attempt();

        // Call heartbeat to see if we have a user session (i.e. user is logged in)
        this.runHeartbeat();
    }

    async runHeartbeat() {
        let resp = await heartbeat();
        resp.msg === 'loggedIn' ? this.handleValidLogin(resp) : this.props.failed();
    }

    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleValidLogin = (userInfo = null) => {
        this.setState({
            failed: false,
            loggedIn: true,
            email: userInfo ? userInfo.email : ''
        });

        this.props.loggedIn();
        refreshFromBaas('basket');
    }

    logOut = async () => {
        let resp = await logout();

        if (resp.success) {
            this.loginCount++;
            this.setState({loggedIn: false});
            this.props.loggedOut();
        }
    }

    handleSubmit = async event => {
        event.preventDefault();

        let resp = await login(this.state.email, this.state.password);
        this.loginCount++;

        if (resp.perms) {
            this.handleValidLogin(resp);
        } else {
            this.setState({failed: true});
            this.props.failed();

            // Show failed message for 1 second
            setTimeout( () => this.setState({failed: false}), 1000);
        }
    }

    render() {
        let {loggedIn, loginAttempt} = this.props.login;

        if (this.state.failed) {
            return  <HeaderWrap props={this.props} loginCount={this.loginCount}>
                        <div className="loggedIn failed">Login failed, please try again</div>
                    </HeaderWrap>
        } else if (loginAttempt) {
            return  <HeaderWrap props={this.props} loginCount={this.loginCount}>
                        <div className="loggedIn attempt">Loging in, please wait</div>
                    </HeaderWrap>
        } else if (loggedIn) {
            return  <HeaderWrap props={this.props} loginCount={this.loginCount}>
                        <label>Welcome <span>{this.state.email}</span></label>
                        <Button block onClick={this.logOut} >Log Out</Button>
                    </HeaderWrap>
        } else { // failed login attempt of didn't try yet
            return  <HeaderWrap props={this.props} loginCount={this.loginCount} >
                        <form onSubmit = {this.handleSubmit}>
                          <FormGroup controlId="email" bssize="large">
                            <label>Email/Login:</label>
                            <FormControl autoFocus type="text"
                                value   = {this.state.email}
                                onChange= {this.handleChange}
                            />
                          </FormGroup>
                          <FormGroup controlId="password" bssize="large">
                            <label>Password:</label>
                            <FormControl type="password"
                                value = {this.state.password}
                                onChange = {this.handleChange}
                            />
                          </FormGroup>
                          <Button block bssize="large" type="submit"
                                disabled = {!this.validateForm()}
                          >
                            Login
                          </Button>
                        </form>
                    </HeaderWrap>
        }
    }
}
