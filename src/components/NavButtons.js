import React from "react";

function NavButtons(props) {

    return <div className="outterWrap buttons">
            <div className="twoFlex plain">
                <button id="undoLast" className="active">
                    <i className="fa fa-undo" />
                </button>
                <button id="redoLast" className="active">
                    <i className="fa fa-redo" />
                </button>
            </div>
            <div className="twoFlex plain">
                <button id="backward5" className="active">
                    <i className="fa fa-angle-double-left" />
                </button>
                <button id="forward5" className="active">
                    <i className="fa fa-angle-double-right" />
                </button>
            </div>

            <div className="twoFlex plain">
                <button id="toStartButtonIcon" className="active">
                    <i
                        className="fa fa-fast-backward tooltip" 
                    >
                        <div className="tooltiptext up">To Start</div>
                    </i>
                </button>
                <button id="replayButtonIcon" className="active">
                    <i
                        className="fa fa-fast-forward tooltip" 
                    >
                        <div className="tooltiptext up">To End</div>
                    </i>
                </button>
            </div>
            <hr />
        </div>
}

export {NavButtons};
