import React, { Component, createRef}     from "react";
import {Subject as TickSource, fromEvent} from 'rxjs';

import "./style/Timer.less";

const timerSounds = {
    tick: new Audio('./assets/tick.mp3'), 
    ding: new Audio('./assets/ding.mp3'),
    bomb: new Audio('./assets/bomb.mp3')
};

// 
// timeState - a singleton responsible for keeing track of current timer render params
//
class TimeState {
    constructor() {
        this.time        = 0;     // miliseconds
        this.clockPeriod = 30000; // miliseconds

        this.paused  = true;
        this.mounted = true;
        this.ticks   = new TickSource();
        this.currentState = {
            mins: 0,
            second: 0,
            tenth: 0,
            period: this.clockPeriod/1000,
            firstPeriod: true
        };
         
        this.pausedAt  = new Date().getTime(); // last time the timer was paused
        this.startedAt = new Date().getTime(); // last time the timer was (re)started
    }

    mount() {
        this.mounted = true;
        this.reset();
        this.runTicks();
        return this.ticks;
    }

    unmount() {
        this.mounted = false; // shut down timer when unmouting component
        this.paused  = true;
    }

    //
    // run - run/pause tiemer
    //
    run = () => {
        // Restart the state object
        this.currentState.atEnd && this.reset();

        let now = new Date().getTime();
        
        if ((this.paused = !this.paused) === true) {
            this.pausedAt = now;
        } else {
            this.startedAt += (now - this.pausedAt); // Add the pause duration to startedAt
        }
    }

    //
    // updateState  - keeps track of the current timer params
    //
    //              - clock time is a difference from current time and the time when the clock started
    //                after a pause the pause period is added to startedAt
    //
    updateState() {
        let time   = new Date().getTime() - this.startedAt;
        let mins   = parseInt(time/60000);
        let second = parseInt(time/1000)%60;
        let tenth  = parseInt(time/100)%10;
        let period = this.clockPeriod/1000;

        this.currentState = {
            run:         !this.paused,
            time:        time,
            tenth:       tenth,
            second:      second,
            mins:        mins,
            period:      period,
            ratio:       time < period*1000 ? time/period/10 : (time - period*1000)/period/10,
            atEnd:       time >= this.clockPeriod * 2 || false,
            firstPeriod: time < this.clockPeriod
        };
        this.ticks.next(this.currentState);
    }

    runTicks = () => {
        // this.mounted === false - effectively turns off recursive cycle (shuts down the timer)
        this.mounted && window.requestAnimationFrame(this.runTicks);

        if (!this.paused && this.mounted) {
            this.updateState();

            if (this.currentState.atEnd)
                this.paused = true;
        }
    }

    setPeriod(period = this.clockPeriod/1000) { 
        this.clockPeriod = period * 1000; 
        return this.currentState;
    }

    reset = () => { 
        this.startedAt = new Date().getTime();
        this.pausedAt  = new Date().getTime(); 
        this.updateState();
    }
};

const Sliders = (props) => {
    let {firstPeriod, ratio} = props.timeState.currentState;

    const reg = () => ({height: !firstPeriod ? '100%' : `${ratio}%`});
    const ext = () => ({height: firstPeriod  ?   '0%' : `${ratio}%`});

    return (
            <div>
                <div className="progress"          style={reg()}></div>
                <div className="progress extended" style={ext()}></div>
                <div className="shadow"></div>
                <div className="shadow extended"></div>
            </div>
    )
}

// Timer - Displays each second while counting from 0 to Time Limit * 2
//       - from 0 to Time Limit - regular time
//       - from Time Limit to Time Limit *2 - extended time
//
//  After Time Limit * 2 has been reached the timer stops and a bomb explodes (if sound is on)
//

class Timer extends Component {

    constructor(props){
        super(props);
        this.timeState = props.timeState;

        this.activeProgress = true; // it should remain 'true' when component is mounted
        this.previousSecond = 0;    // Value of the previous second to see if we should update clock

        //  this.state
        //
        //      soundOn - off if 'false'
        // 
        //      finally set the timer period in this.timeState singleton
        //      and assign all constructor properties to 'this.state'
        this.state = {soundOn: true, ...this.timeState.setPeriod()}; 
        this.inputRef = createRef();
    }

    componentDidMount() {
        // mount and subscribe to timer tick events in this.timeState
        this.timeState.mount().subscribe(this.runClock);

        fromEvent(this.inputRef.current, 'click').subscribe( this.handlePeriodInput );
        fromEvent(this.inputRef.current, 'blur' ).subscribe( this.handlePeriodInput );
    }

    componentWillUnmount() {
        this.timeState.unmount()
    }

    handlePeriodInput = (event) => {

        switch (event.type) {
            case 'click': 
                this.timeState.paused || this.timeState.run(); // pause if running
                this.timeState.reset(); 
                event.target.select();
                break;
            case 'blur' :
                this.setPeriod( this.inputRef.current.value );
                break;
            default: break;
        }
    }

    playSound = () => {
        let {soundOn, mins, second, period, firstPeriod} = this.state;
        second += mins * 60;

        let nearEnd = second >= (firstPeriod ? period : period * 2) - 10;
        let makeSound = second%parseInt(period/3) === 0;

        let audio = 'tick';
        if      (second === period)   audio = 'ding';
        else if (second === period*2) audio = 'bomb';

        soundOn && second > 0 && (makeSound || nearEnd) && timerSounds[audio].play();
    }

    toggleSound = () => this.setState({soundOn: !this.state.soundOn});

    clockStyle = () => {
        let clockClass = this.state.firstPeriod ? ''            : ' extended';
        clockClass    += this.state.mins        ? ' hasMinutes' : '';
        clockClass    += this.state.run         ? ''            : ' paused';
        return clockClass;
    }

    formatSeconds = () => {
        let seconds = Math.max(this.state.second%60, 0);
        if (seconds < 10 && this.state.mins) seconds = `0${seconds}`;
        return seconds;
    }

    setPeriod = (periodValue) => { 
        let newPeriod = parseInt( periodValue )

        this.pausedAt = new Date().getTime();
        this.timeState.setPeriod(newPeriod);
    }

    //
    // runClock - is called from the observable (timerState.tick) on every animationFrame
    //
    runClock = (timerState) => {
        this.setState(timerState);
        
        // do this only when seconds change
        this.state.second !== this.previousSecond && this.playSound();
        this.previousSecond = this.state.second; 
    }

    render() {
        let {mins, tenth, atEnd, period, soundOn} = this.state;

        return (
            <div className="clock">
                <div> 
                    <div onClick={this.timeState.run}
                         className={'tooltip digits' + this.clockStyle()}
                    >
                        <span className="time">
                            {mins > 0 && !atEnd && <span className="mins">{mins}:</span>}
                            {atEnd ? <i className="fa fa-bomb" /> : this.formatSeconds()}
                        </span>
                        {atEnd || <span className="tens">.{tenth}</span>}

                        <Sliders timeState={this.timeState}/>

                        <div className="tooltiptext">Pause/Resume</div>
                    </div>
                </div>

                <button onClick={this.timeState.reset}>
                    <div className="tooltip">
                        { atEnd ? <i className="fa fa-hourglass-end" />
                                : <i className="fa fa-hourglass-start" />
                        }
                        <div className="tooltiptext up">Reset</div>
                    </div>
                </button>

                <label>Time Limit: </label>
                <input type="text" defaultValue={period} ref={this.inputRef} />

                <button onClick={this.toggleSound} id="soundOnOff">
                    <div className="tooltip">
                        {soundOn ? <i className="fa fa-volume-up" />
                                 : <i className="fa fa-volume-off" />
                        }
                        <div className="tooltiptext up">Sound On/Off</div>
                    </div>
                </button>
            </div>
        )
    }
}

//
// trivial HOC component passing timeState singleton to Timer Component
//
export default class extends Component {
    render() {
        return <Timer timeState={new TimeState()} />
    }
}
