import React, { Component } from "react";
import {Route, NavLink, Switch} from "react-router-dom";
import {store} from './redux-baas';

import Order from './containers/Order';
import Go    from './containers/Go';

import Home  from './components/Home';
import Timer from './components/Timer';
//import Tech  from './components/Tech';
//import Samples from './components/Samples';

import { connect } from 'react-redux'

class Routes extends Component {
    constructor(props) {
        super(props);

        this.state = {
            route: 'home'
        }
        store.dispatch( {type: 'ROUTE_TO', payload: 'home'} );
    }

    handleClick = route => {
        this.setState({route: route});
        store.dispatch( {type: 'ROUTE_TO', payload: route} );
    }

    render() {
        return (
            <div>

              <ul className="menu">
                <li>
                    <NavLink exact activeClassName="active" to="/"
                        onClick={() => this.handleClick('/home')}
                    >Home</NavLink>
                </li>
                <li>
                    <NavLink activeClassName="active" to="/redux-baas"
                        onClick={() => this.handleClick('/redux-baas')}
                    >redux-baas</NavLink>
                </li>
                <li>
                    <NavLink activeClassName="active" to="/go"
                        onClick={() => this.handleClick('/go')}
                    >GO!</NavLink>
                </li>
                <li>
                    <NavLink activeClassName="active" to="/timer"
                        onClick={() => this.handleClick('/timer')}
                    >Timer</NavLink>
                </li>
              </ul>

              <div className="content">
                <Switch>
                  <Route path="/redux-baas" component={Order} />
                  <Route path="/go"         component={Go} />
                  <Route path="/timer"      component={Timer} />
                  <Route                    component={Home} />
                </Switch>
              </div>

            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        route: state.route
    }
}

export default connect(
    mapStateToProps
)(Routes)
