import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
//import {store} from './redux-baas';

ReactDOM.render(<App />, document.getElementById('root'));

serviceWorker.unregister();

/*
(async () => {
    store.dispatch({
        "type":    "CONFIG_DATA",
        "payload": await (await fetch('http://localhost:9000/config')).json()
    });
})();
*/
